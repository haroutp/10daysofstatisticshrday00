﻿using System;

namespace Day00
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Console.WriteLine();
            int n = int.Parse(Console.ReadLine());
            int[] x = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp));
            int[] w = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp));

            int[] XxW = new int[n];
            double totalWeighted = 0;
            double totalWeight = 0;
            for(int i = 0; i < n; i++){
                XxW[i] = x[i] * w[i];
                totalWeighted += (double)XxW[i];
                totalWeight += (double)w[i];
            }
            var s = string.Format("{0:##.0}", totalWeighted / totalWeight);
            Console.WriteLine(s);
        }
    }
}
